import Vue from 'vue'
import Router from 'vue-router'
import GameMenu from '@/components/GameMenu.vue'
import GameOver from '@/components/GameOver.vue'
import GamePlay from '@/components/GamePlay.vue'
import QuizQuestion from '@/components/QuizQuestion.vue'
import QuizResults from '@/components/QuizResults.vue'


Vue.use(Router)

export default new Router({
    routes: [
        {
            path:'/gameover',
            name: 'GameOver',
            component: GameOver
        },
        {
            path:'/gameplay',
            name: 'GamePlay',
            component: GamePlay
        },
        {
            path:'/quizquestion',
            name: 'QuizQuestion',
            component: QuizQuestion
        },
        {
            path:'/quizresults',
            name: 'QuizResults',
            component: QuizResults
        },
        {
            path:'/',
            name: 'GameMenu',
            component: GameMenu
        },
        

    ]
})