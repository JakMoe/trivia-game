import Vue from "vue";
import router from './router'
import VueResource from 'vue-resource'
//import './assets/bootstrap.min.css'
import App from "./App.vue";
import { store } from './store/store.js'


Vue.use(VueResource)

Vue.config.productionTip = false;


new Vue({
  store: store,
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
